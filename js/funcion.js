titulo=["#","Numero de Control","Nombre","Edad"];

var datos = [
    ["1","101","Cesar","22"],
    ["2","102","Juan","34"],
    ["3","103","Carlos","28"],
    ["4","104","Adriana","28"],
    ["5","104","Mari","23"]
    ];
    

function tabla_dinamica(titulo,datos){
var tabla = document.createElement("table");
var head = document.createElement("thead");
tabla.appendChild(head);
for (var i=0; i<titulo.length;i++){
    var textNode = document.createTextNode(titulo[i])
    var th=document.createElement("th");
    th.appendChild(textNode);
    head.setAttribute("id","titulos");
    head.appendChild(th);
}

var cuerpo = document.createElement("tbody");
for (var i=0; i<datos.length; i++) {
    var hilera = document.createElement("tr");
    for (var j = 0; j < datos[i].length; j++) {
      var celda = document.createElement("td");
      var textoCelda = document.createTextNode(datos[i][j]);
      celda.appendChild(textoCelda);
      hilera.appendChild(celda);
    }
    cuerpo.appendChild(hilera);
}
tabla.appendChild(cuerpo);
document.body.appendChild(tabla);
tabla.setAttribute("border","1");
}


tabla_dinamica(titulo,datos);